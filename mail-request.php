<?php
	ob_start();
	session_start();
	echo "<meta charset=\"utf-8\">";

	if(strpos($_POST['name'],"http") > 0){ echo "<meta http-equiv=\"refresh\" content=\"0; url = './'\" >"; exit(); }
	if(($_SESSION['verify_code'] == $_POST['verify_code']) && (!empty($_SESSION['verify_code'])) && !empty($_POST['email'])) {
		#---- Send Email From Web Owner  To Contact ----#
		#---- Set Head for Mail Html. ----#
		$companyname = "Melia Phuket Karon Residences";
		$from_fullname = $_POST['name'];
		$from_mail = $_POST['email'];

		//$to_mail = "madee@phuketsolution.com";
		$to_mail = "norbert@meliaphuketkaronresidences.com";
		
		#---- SEND EMAIL : FROM SYSTEM TO CUSTOMER
		#---- To send HTML mail, you can set the Content-type header. ----#
		$head  = "MIME-Version: 1.0\r\n";
		$head .= "Content-type: text/html; charset=utf-8\r\n";
		$head .= "From: norbert@meliaphuketkaronresidences.com\n";
		//$head .= "To: ".$to_mail."\n";
		//$head .= "cc: ".$from_mail."\n";
		//$head .= "cc: \"".$from_real."\" <".$from_mail.">\n";
		$head .= "X-Priority: 1 (High)\n";
		$head .= "X-Mailer: <meliaphuketkaronresidences.com>\n";
		$head .= "MIME-Version: 1.0\n";
		
		#---- SET TEXT
		$message  = "<font style='font-family: Tahoma; font-size:20px;'>------------------------------------- <br />";
		$message .= "<b>Request For Download Our E-Brochure</b> <br />";
		$message .= "------------------------------------- <br />";
		$message .= "Name : <b>".$from_fullname."</b> <br />";
		$message .= "Email : <b>".$_POST['email']."</b> <br />";
		$message .= "------------------------------------- <br /></font>";
		
		//$message = nl2br($message);
		
		// echo $message;
		// exit();
		
		#---- SEND EMAIL
		if( (mail($to_mail,"Request For Download Our E-Brochure",$message,$head,"-fnorbert@meliaphuketkaronresidences.com") ) == false ){
			echo '<script>alert("Your message was unsuccessful. Please contact the staff.")</script>';
			echo "<meta http-equiv=\"refresh\" content=\"0; url = './index.html'\" >";
			exit;
		}


		#---- SENT E-Brochure ----#

		$to_mail = $_POST['email'];
		
		#---- SEND EMAIL : FROM SYSTEM TO CUSTOMER
		#---- To send HTML mail, you can set the Content-type header. ----#
		/*
		$head  = "MIME-Version: 1.0\r\n";
		$head .= "Content-type: text/html; charset=utf-8\r\n";
		$head .= "From: norbert@meliaphuketkaronresidences.com\n";
		//$head .= "To: ".$to_mail."\n";
		//$head .= "cc: ".$from_mail."\n";
		//$head .= "cc: \"".$from_real."\" <".$from_mail.">\n";
		$head .= "X-Priority: 1 (High)\n";
		$head .= "X-Mailer: <meliaphuketkaronresidences.com>\n";
		$head .= "MIME-Version: 1.0\n";
		*/
		
		#---- SET TEXT
		$message  = "<font style='font-family: Tahoma; font-size:20px;'>------------------------------------- <br />";
		$message .= "<b>Dear “".$from_fullname."”,</b> <br />";
		$message .= "------------------------------------- <br />";
		$message .= "Welcome to Phuket’s finest newest Internationally branded resort residences - Melia Phuket Karon Residences. Please find attached our e-brochure. We hope you enjoy it. <br/><br/>
		Prices for our 56.4 sqm one bedroom residences start at <b>8.3m THB</b> for the mountain view units and only <b>9.1m THB</b> for our stunning panoramic sea view units. Add 400,000 THB for the mandatory furniture package if you would like to take advantage our Melia Hotels and Resorts professionally managed Rental Programs, including our investor friendly 3 year 7% annual rental guarantee.<br/><br/>
		Prices for our spacious 88.5 sqm 2 bedroom sea view units are <b>12.9m THB</b>, and add 600,000 THB for the furniture package.<br/><br/>
		Our luxurious 3 bedroom sea view pool villas with over 230 sqm of living areas start at <b>29m THB</b>, and add 1.5m THB for the furniture package.<br/><br/>
		If you would like more product or investing and rental guarantee information , please do not hesitate to hit reply and ask for more details.<br/><br/>
		We look forward to hearing back from you,<br/><br/>";
		//$message .= "https://meliaphuketkaronresidences.com/brochure/MeliaExtendedDigitalBrochure.pdf <br /><br />";
		//$message .= "We look forward to hearing back from you, <br /><br />";
		$message .= "Norbert Zuber<br />
					Sales Director<br />
					Melia Phuket Karon Residences<br />
					Mob. +66 81 0803184<br />
					Web: meliaphuketkaronresidences.com<br />";
		$message .= "------------------------------------- <br /></font>";

		$strSid = md5(time());
		$mimeBoundary = "==Multipart_Boundary_x{$strSid}x";
		$emessage = "";
		
		$head = "";
		//$head .= "From: \"".$from_real."\" <".$from_mail.">\r\n";
		$head .= "From: norbert@meliaphuketkaronresidences.com\n";
		//$head .= "Reply-To: ".$replyto."\r\n";
		$head .= "MIME-Version: 1.0\n";
		//$head .= "Content-Type: multipart/mixed; boundary=\"".$uid."\"\n\n";
		$head .= "Content-Type: multipart/mixed; boundary=\"".$mimeBoundary."\"\n\n";
		//$head .= "This is a multi-part message in MIME format.\n\n";
		$emessage .= "--".$mimeBoundary."\n";
		$emessage .= "Content-type: text/html; charset=UTF-8\n";
		$emessage .= "Content-Transfer-Encoding: 7bit\n\n";
		$emessage .= $message."\n\n";
		$emessage .= "--".$mimeBoundary."\n";

		$filename = "MeliaExtendedDigitalBrochure-v7.pdf";
		$file = "./brochure/MeliaExtendedDigitalBrochure-v7.pdf";
		$fileopen = fopen($file,'rb');
		$content = fread($fileopen,filesize($file)); 
		fclose($fileopen);
		//$content = file_get_contents($file);
		$content = chunk_split(base64_encode($content));
		//$uid = md5(uniqid(time()));
		//$name = basename($file);

		$emessage .= "Content-Type: application/octet-stream; name=\"".$filename."\"\n"; 
		$emessage .= "Content-Disposition: attachment; filename=\"".$filename."\"\n";
		$emessage .= "Content-Transfer-Encoding: base64\n\n".$content."\n\n";
		$emessage .= "--".$mimeBoundary."--\n";
		
		//$message = nl2br($message);
		
		// echo $message;
		// exit();
		
		#---- SEND EMAIL
		if( (mail($to_mail,"Request For Download Our E-Brochure",$emessage,$head,"-fnorbert@meliaphuketkaronresidences.com") ) == false ){
			echo '<script>alert("Your message was unsuccessful. Please contact the staff.")</script>';
			echo "<meta http-equiv=\"refresh\" content=\"0; url = './index.html'\" >";
			exit;
		}

		echo '<script>alert("Your request has been successfully sent. We will be in touch with you shortly. Thank you.")</script>';
		unset($_SESSION['verify_code']);
		echo "<meta http-equiv=\"refresh\" content=\"0; url = './index.html'\" >";
		
	} else {
		echo '<script>alert("Invalid verify code.")</script>';
		echo "<meta http-equiv=\"refresh\" content=\"0; url = './index.html'\" >";
	}
?>