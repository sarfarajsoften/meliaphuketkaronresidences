<?php
	ob_start();
	session_start();
	echo "<meta charset=\"utf-8\">";

	if(strpos($_POST['messages'],"http") > 0){ echo "<meta http-equiv=\"refresh\" content=\"0; url = './'\" >"; exit(); }
	if(($_SESSION['verify_code'] == $_POST['verify_code']) && (!empty($_SESSION['verify_code'])) && !empty($_POST['email'])) {
		#---- Send Email From Web Owner  To Contact ----#
		#---- Set Head for Mail Html. ----#
		$companyname = "Melia Phuket Karon Residences";
		$from_fullname = $_POST['name'];
		$from_mail = $_POST['email'];

		//$to_mail = "boss@phuketsolution.com";
		$to_mail = "sales@meliaphuketkaronresidences.com";
		
		#---- SEND EMAIL : FROM SYSTEM TO CUSTOMER
		#---- To send HTML mail, you can set the Content-type header. ----#
		$head  = "MIME-Version: 1.0\r\n";
		$head .= "Content-type: text/html; charset=utf-8\r\n";
		$head .= "From: sales@meliaphuketkaronresidences.com\n";
		//$head .= "To: ".$to_mail."\n";
		//$head .= "cc: ".$from_mail."\n";
		//$head .= "cc: \"".$from_real."\" <".$from_mail.">\n";
		$head .= "X-Priority: 1 (High)\n";
		$head .= "X-Mailer: <meliaphuketkaronresidences.com>\n";
		$head .= "MIME-Version: 1.0\n";
		
		#---- SET TEXT
		$message  = "<font style='font-family: Tahoma; font-size:20px;'>------------------------------------- <br />";
		$message .= "<b>Contact</b> <br />";
		$message .= "------------------------------------- <br />";
		$message .= "Name : <b>".$from_fullname."</b> <br />";
		//$message .= "Address : <b>".$_POST['address']."</b> <br />";
		$message .= "Email : <b>".$_POST['email']."</b> <br />";
		$message .= "Phone : <b>".$_POST['phone']."</b> <br />";
		$message .= "Message : <b>".$_POST['messages']."</b> <br />";
		$message .= "------------------------------------- <br /></font>";
		
		//$message = nl2br($message);
		
		//echo $message;
		//exit();
		
		#---- SEND EMAIL
		if( (mail($to_mail,"Contact",$message,$head,"-fsales@meliaphuketkaronresidences.com") ) == false ){
			echo '<script>alert("Your message was unsuccessful. Please contact the staff.")</script>';
			echo "<meta http-equiv=\"refresh\" content=\"0; url = './index.html'\" >";
			exit;
		}

		echo '<script>alert("Your contact has been successfully sent. We will be in touch with you shortly. Thank you.")</script>';
		unset($_SESSION['verify_code']);
		echo "<meta http-equiv=\"refresh\" content=\"0; url = './index.html'\" >";
		
	} else {
		echo '<script>alert("Invalid verify code.")</script>';
		echo "<meta http-equiv=\"refresh\" content=\"0; url = './index.html'\" >";
	}
?>