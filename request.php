<!DOCTYPE html>
<html lang="en">

  <head>
<title>Karon Beach Resort; Luxury Sea View Condo Development Phuket</title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="google-site-verification" content="0Zqfe4gHU86Z3e793EpoFicrMJomWUNJXXWkpqLjBeU" />
    <meta name="keywords" content="Melia Karon,Melia phuket,Sea View Villas for Sale Phuket,Sea View Condos for Sale Phuket,Investment Property for Sale Phuket,Resort Property for Sale Phuket,Hotel Residences for Sale Phuket,Guaranteed Rental Returns Phuket,Luxury Villas for Sale Phuket,Luxury Condos for Sale Phuket,Property for Sale Karon,Real Estate for Sale Karon">
<link rel="shortcut icon" href="images/favicon.png">
<link rel="stylesheet" href="css/bundle.css">
<link rel="stylesheet" href="css/hody-icons.css">
<link rel="stylesheet" href="css/style.css">
<link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
<link href="https://fonts.googleapis.com/css?family=Raleway:300,400,500" rel="stylesheet" type="text/css">
<link href="https://fonts.googleapis.com/css?family=Quattrocento:400,700" rel="stylesheet" type="text/css">
<link href="https://fonts.googleapis.com/css?family=Tangerine:400" rel="stylesheet" type="text/css">
<!--[if lt IE 9]>
<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
<![endif]-->
</head>

<body>
<!-- Preloader-->
<div id="loader">
<div class="centrize">
<div class="v-center">
<div id="mask">
<span></span>
</div>
</div>
</div>
</div>
<!-- End Preloader-->
<!-- Navigation Bar-->
<header id="topnav">
<div class="container">
<!-- Logo container-->
<div class="logo">
<a href="index.html">
<img class="logo-light" src="images/logo-melia-white.png" alt="">
<img class="logo-dark" src="images/logo-melia.png" alt="">
</a>
</div>
<!-- End Logo container-->
<div class="menu-extras">
<div class="menu-item">
<div class="widget clearfix" style="padding-top:20px;">
<ul class="social-list">
<li class="social-item-facebook"><a target="_blank" href="https://www.facebook.com/meliaphuketkaron"><i class="hc-facebook"></i></a></li>
<li class="social-item-twitter"><a target="_blank" href="#"><i class="hc-twitter"></i></a></li>
<li class="social-item-instagram"><a target="_blank" href="#"><i class="hc-instagram"></i></a></li>
<li class="social-item-youtube"><a target="_blank" href="#"><i class="hc-youtube"></i></a></li>
</ul>
<div style="background-color:#e1ddd9; opacity:0.8; padding:5px; font-size:11px; color:#fff; border-radius:10px; text-align:center; letter-spacing:1px; margin-bottom:5px;"><a href="contact.php">ENQUIRE</a></div>
</div>
</div>
<div class="menu-item toggle-nav">
<!-- Mobile menu toggle-->
<a class="menu-toggle" href="#">
<div class="toggle-inner"></div>
</a>
<!-- End mobile menu toggle-->
</div>
</div>
<div class="aligned-menu" id="navigation">
<ul class="navigation-menu nav">
<li><a href="index.html">Home</a></li>
<li><a href="about.html">About Us</a></li>
<li class="menu-item-has-children"><a href="#">The Residences</a>
<ul class="submenu">
<li><a href="1bedroom-residences.html">1 Bedroom Residences</a></li>
<li><a href="2bedroom-residences.html">2 Bedroom Residences</a></li>
<li><a href="3bedroom-ocean-view-pool-villas.html">3 Bedroom Ocean View Pool Villas</a></li>
</ul>
</li>
<li><a href="melia.html">Melia</a></li>
<li><a href="location.html">Location</a></li>
<li><a href="news.html">News</a></li>
<li><a href="contact.php">Contact</a></li>
</ul>
</div>
</div>
</header>
<!-- End Navigation Bar-->
    
<section class="page-title parallax-bg">
<div class="row-parallax-bg">
<div class="parallax-wrapper">
<div class="parallax-bg-element">
<img src="images/bg-location.jpg" alt="">
</div>
</div>
</div>
<div class="centrize">
<div class="v-center">
<div class="container">
<div class="title">
<h1 class="upper">Download Our E-Brochure</h1>
<h4>Melia Phuket Karon Residences</h4>
</div>
</div>
</div>
</div>
<div class="section-scrolling"><a id="scroll-section" href="#"><i class="hc-angle-down"></i></a>
</div>
</section>

<script type="text/javascript">
	function checkform(){
		var name = document.frm_contact.name.value;
		var emailFilter = /^[_a-zA-Z0-9-]+(\.[_a-zA-Z0-9-]+)*\@[a-zA-Z0-9-]+(\.[a-zA-Z0-9-]+)*(\.([a-zA-Z]){2,4})$/;
		var str = document.frm_contact.email.value;
		var verify_code = document.frm_contact.verify_code.value;
										
		if(name == ''){ alert("Please enter your name."); return false; }
		if(str == ''){ alert ("Please enter your email."); return false; }
		if(!(emailFilter.test(str))){ alert ("Your email is incorrect."); return false; }
		if(verify_code == ''){ alert("Please enter the verify code."); return false; } /* alert("Verify Code is required."); */
		return ture;
	}
</script>

<section class="grey-bg pt-0">
<div class="container">
<div class="section-content" data-negative-margin="100">
<div class="box with-box-shadow">
<div class="row">
<h4 align="center">Send Request For Download Our E-Brochure</h4>
<div class="col-sm-12 col-md-8 col-md-offset-2">
<form action="mail-request.php" method="POST" id="frm_contact" name="frm_contact" onsubmit="return checkform();">

<div class="form-group">
<input class="form-control" type="text" name="name" id="name" placeholder="Your Name **" data-required>
</div>
<!--<div class="form-group">
<input class="form-control" type="text" name="address" id="address" placeholder="Your Address **" data-required>
</div>-->
<div class="form-group">
<input class="form-control" type="text" name="email" id="email" placeholder="Your Email **" data-required>
</div>

<div class="form-group">
<img src="./captcha/captcha.php" align="absmiddle" /><br /><br />
<input type="text" id="verify_code" name="verify_code" class=" form-control add_bottom_30" placeholder="Verify Code" maxlength="5">
</div>
<div class="form-group">
<input class="btn btn-color-out" type="submit" value="Send Request">
</div>
</form>
</div>

</div>
</div>
</div>
</div>
</section>

    
<footer id="footer-widgets">
<div class="container">
<div class="row">
<div class="col-md-4 ov-h">
<div class="widget clearfix">
<h5>Get In Touch</h5>
<div class="textwidget">
<p>
<h5>Melia Phuket Karon Residences</h5>
<span>408 Patak Road, Karon, Muang</span>
<br><span>Phuket 83000, Thailand</span>
<br><span>sales@meliaphuketkaronresidences.com</span>
</p>
<h5>Mobile +66 81 080 3184</h5>
<h5>Mobile +66 85 886 7687</h5>
<p>
<a class="mt-15 btn btn-color btn-sm" href="contact.php">Contact Us</a>
</p>
</div>
</div>
</div>

<div class="col-md-4">
<div class="widget clearfix">
<h5>The Residences</h5>
<div class="menu-footer-1-container">
<ul class="menu">
<li><a href="about.html">About Us</a></li>
<li><a href="1bedroom-residences.html">1 Bedroom Residences</a></li>
<li><a href="2bedroom-residences.html">2 Bedroom Residences</a></li>
<li><a href="3bedroom-ocean-view-pool-villas.html">3 Bedroom Ocean View Pool Villas</a></li>
<li><a href="melia-html">Melia</a></li>
<li><a href="news.html">News</a></li>
<li><a href="disclaimer.html">Disclaimer</a></li>
</ul>
</div>
</div>
</div>

<div class="col-md-4 ">
<div class="row">
<div class="col-md-12">
<div class="widget clearfix">
<h5>Follow Us</h5>
<ul class="social-list">
<li class="social-item-facebook"><a target="_blank" href="https://www.facebook.com/meliaphuketkaron"><i class="hc-facebook"></i></a></li>
<li class="social-item-twitter"><a target="_blank" href="#"><i class="hc-twitter"></i></a></li>
<li class="social-item-instagram"><a target="_blank" href="#"><i class="hc-instagram"></i></a></li>
<li class="social-item-youtube"><a target="_blank" href="#"><i class="hc-youtube"></i></a></li>
</ul>
</div>
</div>
</div>
</div>
</div>
</div>
</footer>
<footer id="footer">
<div class="container">
<div class="copy-text">
<p>© Melia Phuket Karon Residences. All rights reserved. |  Powered by <a href="https://www.phuketsolution.com" target="_blank">Phuket Solution.</a></p>
</div>
</div>
</footer>
 
<div class="go-top"><a href="#top"><i class="hc-angle-up"></i></a></div>
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/bundle.js"></script>
<script type="text/javascript" src="js/SmoothScroll.js"></script>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCNGOsBBZo9vf0Tw4w6aJiilSTFVfQ5GPI"></script>
<script type="text/javascript" src="js/main.js"></script>
</body>

</html>